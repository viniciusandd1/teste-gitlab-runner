<?php

require 'vendor/autoload.php';

use \FaaPz\PDO\Database;

$host     = getenv('DATABASE_HOST');
$port     = getenv('DATABASE_PORT');
$username = getenv('DATABASE_USERNAME');
$password = getenv('DATABASE_PASSWORD');
$path     = getenv('DATABASE_PATH');
$dbname   = '07350773000147';
$charset  = "utf8";
$dsn      = "firebird:dbname=".$host."/".$port.":".$path.$dbname.".fdb;charset=".$charset;
$database = new Database($dsn, $username, $password);

$select_statement = $database->select()->from('pgto_condicoes');
$stmt = $select_statement->execute();
while ($row = $stmt->fetch()) {
    print_r($row);
}