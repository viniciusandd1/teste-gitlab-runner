FROM php:7.0-apache

RUN apt-get update && apt-get install -y firebird-dev

ADD . /var/www/html/

RUN a2enmod rewrite

RUN docker-php-ext-install pdo_firebird

RUN curl -sS https://getcomposer.org/installer | php && \ 
    chmod +x composer.phar && \     
    mv composer.phar /usr/local/bin/composer
RUN composer install --no-dev

EXPOSE 80

ENV DATABASE_HOST="192.168.0.14"
ENV DATABASE_PORT="3080"
ENV DATABASE_USERNAME="sysdba"
ENV DATABASE_PASSWORD="masterkey"
ENV DATABASE_PATH="/var/lib/firebird/2.5/data/"
